package solutions.prodigi.springretryexample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GreetingsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GreetingsService.class);

    private final GreetingMessageService greetingMessageService;

    @Autowired
    public GreetingsService(GreetingMessageService greetingMessageService) {
        this.greetingMessageService = greetingMessageService;
    }

    public String greetMe(String name) {
        LOGGER.info("Greet me is called with name: {}", name);
        return String.format("%s %s", greetingMessageService.getGreetingMessage(), name);
    }
}
