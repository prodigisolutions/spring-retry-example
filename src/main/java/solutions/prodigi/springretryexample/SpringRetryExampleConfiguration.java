package solutions.prodigi.springretryexample;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.retry.annotation.EnableRetry;

@SpringBootConfiguration
@ComponentScan
@EnableRetry
public class SpringRetryExampleConfiguration {
}
