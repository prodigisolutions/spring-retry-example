package solutions.prodigi.springretryexample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

@Service
public class GreetingMessageService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GreetingMessageService.class);

    @Retryable
    public String getGreetingMessage() {
        LOGGER.info("Trying to get a greeting message");
        throw new RuntimeException("Failed doing the important stuff");
    }

    @Recover
    public String recover(Throwable t) {
        LOGGER.error("Recovering from {}", t.getMessage());
        return "Hello";
    }
}
