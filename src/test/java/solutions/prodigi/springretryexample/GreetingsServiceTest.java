package solutions.prodigi.springretryexample;

        import org.junit.Test;
        import org.junit.runner.RunWith;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.boot.test.context.SpringBootTest;
        import org.springframework.test.context.junit4.SpringRunner;

        import static org.hamcrest.CoreMatchers.is;
        import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GreetingsServiceTest {

    @Autowired
    private GreetingsService greetingsService;

    @Test
    public void canGreetMe() throws Exception {
        assertThat(greetingsService.greetMe("baran"), is("Hello baran"));
    }

}